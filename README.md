# rxjsdom-import

Este WebComponent integra https://github.com/Reactive-Extensions/RxJS-DOM como
elemento ao Polymer. 

Nesta versão integra apenas:

* rx.lite.js
* rx.dom.js

A medida que mais APIs sejam necessárias, elas poderão ser integradas também.
Podemos versionar para obter controle fino de features.

Para mais detalhes sobre __Reactive Programming__ veja o 
link [The introduction to Reactive Programming you've been missing](https://gist.github.com/staltz/868e7e9bc2a7b8c1f754)
ou [Este](https://egghead.io/series/introduction-to-reactive-programming)

__Exemplo de uso__ 

Autocomplete baseado em pesquisa na Wikipedia :

    <!doctype html>
    <html>
    <head>
      <meta charset="utf-8">
      ...
      <script src="../rxjsdom-import/rxjsdom-util.js"></script>
      <script src="../webcomponentsjs/webcomponents-lite.js"></script>
      <link rel="import" href="../rxjsdom-import/rxjsdom-import.html">
      ...
    </head>
    <body>
      <rxjsdom-import></rxjsdom-import>
      <script>console.log('••• Iniciando autocomplete');</script>
      <input id="textInput" type="text"></input>
      <ul id="results"></ul>
      <script src="rxjsdom/autocomplete.js"></script>
    </body>
    </html>

Onde rxjsdom/autocomplete.js é uma `immediately invoked function` :

    (function(){
      function searchWikipedia(term) {
        var url = 'http://pt.wikipedia.org/w/api.php?action=opensearch&format=json&search='
          + encodeURIComponent(term) + '&callback=JSONPCallback';
        return Rx.DOM.jsonpRequest(url);
      }
      function clearSelector (element) {
        while (element.firstChild) {
          element.removeChild(element.firstChild);
        }
      }
      function createLineItem(text) {
          var li = document.createElement('li');
          li.innerHTML = text;
          return li;
      }
      var textInput = document.getElementById('textInput');
      console.log('Início');
      function doIt() {
        var throttledInput;
        // TODO: ver como podemos garantir que Rx.DOM já esteja carregado
        (function(){
          throttledInput = Rx.DOM.keyup(textInput)
            .pluck('target','value')
            .filter( function (text) {
                return text.length > 2;
            })
            .debounce(250)
            .distinctUntilChanged();
          var suggestions = throttledInput.flatMapLatest(searchWikipedia);
          var resultList = document.getElementById('results');
          suggestions.subscribe(function (data) {
            var results = data.response[1];
            clearSelector(resultList);
            for (var i = 0; i < results.length; i++) {
              resultList.appendChild(createLineItem(results[i]));
            }
          },
          function (e) {
            clearSelector(resultList);
            resultList.appendChild(createLineItem('Error: ' + e));
          });
        })();
      }

      // Devemos aguardar o Load do Rx
      ensureRxDOMIsReady(function(){
        doIt();
      });
    })(); 
