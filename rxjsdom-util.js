//  Tenho que garantir que o framework Reactive está Pronto  
var ensureRxDOMIsReady = function(callback) {
  var rxNotReady = function() {
    if ('undefined' != (typeof Rx) &&
        'undefined' != (typeof Rx.DOM) &&
        'undefined' != (typeof Rx.DOM.ready)) {
      return false;
    }
    return true;
  }
  var count = 0;
  var delaySubscribe = function(){
    if (rxNotReady()) {
      // verifica num intervalo de 10 milisegundos
      setTimeout(delaySubscribe, 10);
      count++;
    } else {
      console.log('rx is ready in ' + (count * 10) + ' milliseconds.');
      setTimeout(callback, 50);
    }
  }
  delaySubscribe();
};
